#!/usr/bin/python
# coding=UTF-8
# noinspection PySingleQuotedDocstring
"""CopyRight © 2020 Luomuyu.All Rights Reserved."""
"""Version:v1.1.0"""
import requests
import time
import os
import json
import sys
import re
import base64
import urllib
import urllib.request
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
tempname = os.path.isdir("temp")
if tempname == True:
    print("正在清理更新缓存")
    os.removedirs("temp")
config = os.path.isfile("version.ini")
if config == True:
    print("正在启动...")
    os.system("cls")
else:
    print("检测到第一次启动，正在释放文件...")
    vfp = open("version.ini","w",encoding="utf-8")
    print("v1.1.0",file=vfp)
    vfp.close()
    urllinsense = ""#linsense.txt协议文件链接，自行修改
    linsense = "linsense.txt"
    urllib.request.urlretrieve(urllinsense, linsense)
    time.sleep(3)
    print("请确保已阅读《祈Inory直播间封禁器使用规则》")
    print("继续使用即代表您已同意《祈Inory直播间封禁器使用规则》")
    os.system(r"notepad linsense.txt")
    os.system("cls")
vfp = open("version.ini", "r", encoding="utf-8")
version = vfp.read()
vfp.close()
print("正在校验版本")
verurl = ""#version.txt最新版本号文件，自行修改
newversion = requests.ger(verurl)
newver = newversion.text
if newver == "1.1.0":#版本校验
    print("正在等待云端下发cookie")
    time.sleep(1)
    headersurl = ""#cookie.txt云加密下发文件，自行修改
    headers = requests.get(headersurl)
    cfp = open("cookie.ini", "w", encoding="utf-8")
    print(headers.text,file=cfp)
    cfp.close()
    while True:#主循环
        os.system("cls")
        cfp = open("cookie.ini", "r", encoding="utf-8")
        class AesSample(object):
            def __init__(self):
                self.key = ''.encode('utf-8')#32位密钥，自行修改
                self.iv = ''.encode('utf-8')#16位偏移量，自行修改
                self.mode = AES.MODE_CBC
            def decode(self, data):#解密算法
                cipher = AES.new(self.key, self.mode, self.iv)
                base64_decrypted = base64.decodebytes(data.encode('utf-8'))
                una_pkcs7 = unpad(cipher.decrypt(base64_decrypted), AES.block_size, style='pkcs7')
                decrypted_text = str(una_pkcs7, encoding='utf-8')
                return decrypted_text
            def inory(self):
                cookieenc = cfp.read()
                cookie = self.decode(cookieenc)
                print("欢迎使用祈Inory直播间违禁用户封禁器")
                print("本文件仅供巡查组内部使用")
                print("版本："+version)
                print("请输入被封禁人UID")
                banuid = input()
                print("请输入封禁小时数")
                print("支持范围：1-720")
                bantime = input()
                localheaders = cookie
                bilijct = re.findall('bili_jct=+[a-z0-9]*', localheaders)[0]
                csrftoken = re.findall('[0-9a-z]{32,}', bilijct)[0]
                cfp.close()
                biliurl = "https://api.live.bilibili.com/banned_service/v2/Silent/add_block_user"#哔哩哔哩封禁API
                biliheaders = eval(localheaders)
                bilidata = {
                    "roomid": "544625",#真实房间号非短位
                    "block_uid": banuid,
                    "hour": bantime,
                    "csrf_token": csrftoken,
                    "csrf": csrftoken,
                    "visit_id":""
                    }
                biliresponse = requests.post(url=biliurl,headers=biliheaders,data=bilidata)
                jstext = biliresponse.text
                jsonobj = json.loads(jstext)
                status = jsonobj["code"]
                if status == 0:
                    nameapi = "https://api.bilibili.com/x/space/acc/info?mid="+str(banuid)+"&jsonp=jsonp"#哔哩哔哩用户信息API
                    nameresponse = requests.get(url=nameapi,headers=biliheaders)
                    nametext = nameresponse.text
                    nameobj = json.loads(nametext)
                    name = nameobj["data"]["name"]
                    print("用户名["+name+"],UID["+banuid+"]已被封禁"+bantime+"小时!")
                    print("封禁信息："+biliresponse.text)
                    logs = open("logs.txt","a",encoding="utf-8")
                    localtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                    print(localtime+"\n"+"用户名["+name+"],UID["+banuid+"]已被封禁"+bantime+"小时!"+"\n"+"封禁信息："+biliresponse.text+"\n",file=logs)
                    logs.close()
                    time.sleep(3)
                    os.system("cls")
                else:
                    print("封禁失败！")
                    time.sleep(3)
                    os.system("cls")
        enc = AesSample()
        enc.inory()
else:#版本落后
    print("当前版本已弃用，自动更新中")
    os.mkdir("temp")
    old = os.getcwd()
    os.chdir(old + "/temp")
    urlexe = ""#exe编译后更新文件，自行修改
    exename = "blankroom.exe"#更新文件名
    urllib.request.urlretrieve(urlexe, exename)
    urlbat = ""#bat更新脚本，自行修改
    batname = "update.bat"#更新脚本名
    urllib.request.urlretrieve(urlbat, batname)
    os.system("update.bat")#运行更新脚本
