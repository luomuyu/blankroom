import base64
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
class AesSample(object):
    def __init__(self):
        self.key = ''.encode('utf-8')#32位密钥
        self.iv = ''.encode('utf-8')#16位偏移量
        self.mode = AES.MODE_CBC
    def encode(self, data):
        cipher = AES.new(self.key, self.mode, self.iv)
        pad_pkcs7 = pad(data.encode('utf-8'), AES.block_size, style='pkcs7')
        result = base64.encodebytes(cipher.encrypt(pad_pkcs7))
        encrypted_text = str(result, encoding='utf-8').replace('\n', '')
        return encrypted_text
    def test(self):
		cfp = open("cookie.txt", "r", encoding="utf-8")
		data = cfp.read()
        print('加密结果：', self.encode(data))
		cfp.close()
if __name__ == '__main__':
    blog = AesSample()
    blog.test()
